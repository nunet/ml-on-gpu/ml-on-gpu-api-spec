## Algorithms used in the NuNet ML on GPU API

### 1. [Categorizing Resource Types](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/issues/2)

The first and foremost challenge for us was to classify how to distinguish between different resource types. We did it by classifying them into 3:

#### 1. Low Resource Usage

This type corresponds to low-end machines with low-end GPUs.

#### 2. Moderate Resource Usage

This type corresponds medium-end machines with medium-end GPUs.

#### 3. High Resource Usage

This type corresponds to high-end machines with high-end GPUs.

### 2. [Calculating Resource Prices](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/issues/1)

Now that we have the resource types classified, we can set formulae for each resource type, to estimate the resource price. Resource price estimation is basically performed with help of three essential factors:

#### a. Estimated Static NTX

This NTX value is calculated based only on the user's input of the estimated execution time of the computational task and the selected resource type.

#### b. Computed Dynamic NTX

This NTX value is calculated based only on the computed execution time of the computational task and its corresponding power draw.

### c. Delta NTX

**Delta NTX** is either the additional amount higher than the actual price, submitted by the user or the amount of insufficiency that would meet the actual price. Delta denotes the change in amount. This actual price is what we call the **Final NTX** which is adjusted through Delta NTX.

### 3. [Reporting Task Results](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/issues/6)

The function **Upload_Compute_Job_Result()** reports the task's progress every 2 minutes at a permanent link on the web. **Send_Answer_Message()** allows the WebApp to track each task corresponding their requesters through that link.

### 4. [Resuming Incomplete ML Jobs](https://gitlab.com/nunet/ml-on-gpu/ml-on-gpu-service/-/issues/8)

NuNet's Device Management Service(DMS) would resume a job that was interrupted, transfer it to another machine if necessary and restart it with a maximum of a second attempt if it cannot be resumed.
