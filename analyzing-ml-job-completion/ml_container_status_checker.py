import subprocess
import sys

def check_container_status(container_name, estimated_execution_time):
    # Find the container ID for the latest instance of the container
    docker_ps_cmd = f"docker ps --format '{{.ID}}' --filter 'name={container_name}'"
    container_id = subprocess.check_output(docker_ps_cmd, shell=True, text=True).splitlines()[-1]

    # Get the container creation time in nanoseconds
    docker_inspect_created_cmd = f"docker inspect --format='{{.Created}}' {container_id}"
    created = subprocess.check_output(docker_inspect_created_cmd, shell=True, text=True).strip()

    # Convert the creation time to seconds since epoch
    date_cmd = f"date '+%s' --date={created}"
    seconds_since_epoch = subprocess.check_output(date_cmd, shell=True, text=True).strip()

    # Get the current time in seconds since epoch
    current_time = subprocess.check_output("date '+%s'", shell=True, text=True).strip()

    # Calculate the age of the container in minutes (exited or running)
    container_age = (int(current_time) - int(seconds_since_epoch)) // 60

    if container_age >= estimated_execution_time:
        # Get the start time of the container
        docker_inspect_start_cmd = f"docker inspect --format='{{.State.StartedAt}}' {container_id}"
        start_time = subprocess.check_output(docker_inspect_start_cmd, shell=True, text=True).strip()

        # Get the end time of the container
        docker_inspect_end_cmd = f"docker inspect --format='{{.State.FinishedAt}}' {container_id}"
        end_time = subprocess.check_output(docker_inspect_end_cmd, shell=True, text=True).strip()

        # Calculate the difference between start and end time in seconds
        time_diff = int(subprocess.check_output(f"date -d '{end_time}' +%s", shell=True, text=True).strip()) - \
                    int(subprocess.check_output(f"date -d '{start_time}' +%s", shell=True, text=True).strip())

        # Calculate the execution time in minutes
        actual_execution_time = time_diff // 60

        # Check if the container is still running
        docker_ps_filter_cmd = f"docker ps --filter 'id={container_id}'"
        if container_id in subprocess.check_output(docker_ps_filter_cmd, shell=True, text=True):
            # Check if the age of the container (assumed to be exited now) has exceeded the actual execution time
            if actual_execution_time <= container_age:
                return "The job has completed successfully."
            else:
                return "The job is still running."
        else:
            # Check if the container has restarted
            docker_inspect_cmd = f"docker inspect {container_id}"
            container_restart_count = int(subprocess.check_output(f"{docker_inspect_cmd} | jq '.[0].RestartCount'", shell=True, text=True).strip())
            if container_restart_count > 1:
                return "The job has restarted more than once."
            else:
                # Get the exit status code of the container
                docker_inspect_exit_code_cmd = f"docker inspect -f '{{.State.ExitCode}}' {container_id}"
                exit_code = subprocess.check_output(docker_inspect_exit_code_cmd, shell=True, text=True).strip()
                # Check if the exit status code indicates success
                if exit_code == 0:
                    print("The job has completed successfully.")
                else:
                    print(f"The job has failed with exit code {exit_code}.")
