import argparse
import subprocess
import sys
import time
import traceback

# Import the check_output_for_errors function from ml_log_checker.py
from ml_log_checker import check_output_for_errors
# Import the check_container_status function from ml_container_status_checker.py
from ml_container_status_checker import check_container_status

def main(container_name, log_dir, log_prefix, container_name, estimated_execution_time):
    # Check if container is running
    cmd = ['docker', 'inspect', '-f', '{{.State.Running}}', container_name]
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if result.returncode != 0 or result.stdout.decode().strip() != 'true':
        print(f"Error: Container {container_name} is not running")
        sys.exit(1)

    # Start loop to check logs every 5 minutes
    while True:
        # Get current date and time for log file name
        log_suffix = time.strftime("%Y-%m-%d_%H-%M-%S")
        log_file = f"{log_dir}/{log_prefix}-{log_suffix}.log"

        # Get last log position
        if os.path.isfile(log_file):
            last_pos = os.path.getsize(log_file)
        else:
            last_pos = 0

        # Get container logs since last position
        cmd = ['docker', 'logs', '-t', '--since', str(last_pos), container_name]
        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output = result.stdout.decode()

        # Write output to log file
        with open(log_file, 'a') as f:
            f.write(output)

        # Check if container has exited
        cmd = ['docker', 'inspect', '-f', '{{.State.Running}}', container_name]
        result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode != 0 or result.stdout.decode().strip() != 'true':
            print(f"Container {container_name} has exited")
            check_output_for_errors(log_file)  # Call the check_output_for_errors function
            sys.exit(0)

        # Check the log file and wait for 5 minutes before checking logs again
        check_container_status(container_name, estimated_execution_time)
        check_output_for_errors(log_file)  # Call the check_output_for_errors function
        time.sleep(300)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Monitor container logs for errors.')
    parser.add_argument('--container-name', type=str, required=True, help='Name of the container to monitor')
    parser.add_argument('--log-dir', type=str, required=True, help='Directory to store log files')
    parser.add_argument('--log-prefix', type=str, default='log', help='Prefix for log file names')
    parser.add_argument('--ml-container-name', type=str, help='Name of the MLcontainer')
    parser.add_argument('--estimated-time', type=int, help='Estimated execution time obtained from the WebApp')

    args = parser.parse_args()

    main(args.container_name, args.log_dir, args.log_prefix, args.container_name, arg.estimated_execution_time)
