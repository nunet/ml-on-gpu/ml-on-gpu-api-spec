import argparse
import sys
import traceback

def check_output_for_errors(output):
    # Split the output into lines and reverse the order
    lines = output.split('\n')[::-1]
    # Initialize a variable to store the error message (if any)
    error_message = None
    # Loop over all lines of the output
    for line in lines:
        # Check if the line contains the word "error" (case-insensitive)
        if 'error' in line.lower():
            # If it does, set the error message to the line
            error_message = line
            break
        # Check for framework-specific error messages
        elif 'pytorch' in line.lower() and ('backward' in line.lower() or 'grad' in line.lower()):
            error_message = line
            break
        elif 'tensorflow' in line.lower() and ('graph' in line.lower() or 'session' in line.lower()):
            error_message = line
            break
        # Check for Python exceptions using traceback module
        elif 'exception' in line.lower():
            tb = traceback.extract_tb(sys.exc_info()[2])
            error_message = str(tb[-1][1]) + ': ' + str(tb[-1][3])
            break
        # Check for common non-Error exceptions
        elif 'stopiteration' in line.lower():
            error_message = 'StopIteration'
            break
        elif 'keyboardinterrupt' in line.lower():
            error_message = 'KeyboardInterrupt'
            break
        elif 'generatorexit' in line.lower():
            error_message = 'GeneratorExit'
            break
        # Check for segmentation fault
        elif 'segmentation fault' in line.lower():
            error_message = 'Segmentation fault'
            break
        # Check for bus error
        elif 'bus error' in line.lower():
            error_message = 'Bus error'
            break
        # Check for stack overflow
        elif 'stack overflow' in line.lower():
            error_message = 'Stack overflow'
            break
        # Check for illegal instruction
        elif 'illegal instruction' in line.lower():
            error_message = 'Illegal instruction'
            break
        # Check for floating-point exception
        elif 'floating-point exception' in line.lower():
            error_message = 'Floating-point exception'
            break
        # Check for aborted program
        elif 'aborted' in line.lower():
            error_message = 'Aborted'
            break
        # Check for core dumped
        elif 'segmentation fault (core dumped)' in line.lower():
            error_message = 'Segmentation fault (core dumped)'
            break

    # If an error message was found, print it
    if error_message is not None:
        print("Error found in output: {}".format(error_message))
    else:
        print("No errors found in output.")
